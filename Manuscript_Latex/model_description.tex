We are presenting here the result of our exercise of merging together two independent, previously developed kinetic models of photosynthesis, both based on ordinary differential equations (ODEs). 
The first model describes the primary photosynthetic reactions 
through the PETC, leading to the production of ATP and NADPH. 
The CBB cycle is considered as the main consumer of the produced energy and reducing equivalents. Therefore in this model, the downstream metabolism has been simplified to two consuming reactions governed by mass action kinetics. 
It has been developed based on our previous work: the core model of the PETC by Ebenh\"oh \textit{et al.}~\cite{Ebenhoh2014} and the model of high-energy dependent quenching in higher plants developed by Matuszy\'nska \textit{et al.}~\cite{Matuszynska2016}. Using this model, we are able to compute the fluorescence emission under various light protocols, monitor the redox state of the thylakoid and the rate of ATP and NADPH synthesis.

The second model is the Poolman \cite{Poolman2000} implementation of the carbon fixation model by Pettersson and Ryde-Pettersson~\cite{Pettersson1988}, reproduced in our Institute using the \texttt{modelbase} software \cite{Ebenhoh2018}. 
In contrast to the original model~\cite{Pettersson1988}, in the Poolman representation the rapid equilibrium assumptions were not solved explicitly, but instead approximated by mass-action kinetics with very large rate constants.
Solving the system of ODEs allows computation of different carbon fixation rates and reaction activities at varying concentrations of external orthophosphate. 
In the original model, the input of the ETC has been simplified by a single lumped reaction of ATP synthesis ($v_{16}$ in \cite{Pettersson1988}), whilst NADPH has been kept as a constant parameter.

\subsection{Included processes and the stoichiometry}
The model, schematically captured in Figure~\ref{fig:scheme}, comprises of 35 reaction rates and follows the dynamics of 24 independent state variables (see Supplement for a full list of reaction rates and ODEs). In addition, we compute a number of values such as emitted fluorescence or variables derived from conserved quantities. Light is considered as an external, time-dependent variable. 
Since the focus of this model is to study basic system properties, such as the response to relative changes in the light intensity, we did not calibrate our simulations to experimentally measured light intensities. Therefore, in this work light is expressed in $\mu$ mol photons per m$^2$ and second and reflects
the quantity of light efficiently used, but the utilization conversion factor to the Photon Flux Density (PFD) of the incident light is unknown. 
We included two compartments in our model, the thylakoid lumen and the chloroplast stroma. \textbf{Lumen.} The reaction kinetics for oxidised plastoquinone (PQ), oxidised plastocyanin (PC), oxidised ferrodoxin (Fd), lumenal protons (H) and non-phosphorylated antenna (light harvesting complexes) were taken from \cite{Ebenhoh2014}. The four-state description of the quencher activity, based on the protonation of the PsbS protein and activity of the xanthophyll cycle, was taken from our mathematical model of non-photochemical quenching, initially developed to study short-term light memory in plants~\cite{Matuszynska2016}. The previous description of ATP and NADPH consuming reactions is supplemented by the detailed description of the CBB Cycle.
\textbf{Stroma.} Processes of the CBB Cycle have been implemented as in the mathematical model of the Calvin photosynthesis by Poolman \textit{et al.}~\cite{Poolman2000}, based on the original work of Pettersson and Ryde-Pettersson~\cite{Pettersson1988}. The original model reproduces different carbon fixation rates and reaction activities at different concentrations of external orthophosphate, and includes the conversion of fixed carbon into either triose phosphates or sugar and starch. 
This model has been parametrised for CO$_2$ saturating conditions and we kept the same assumption for all our analyses.
The previous description of ATP synthesis is supplemented in our model with the new rate $v_\text{ATPsynthase}$, which depends on the proton motive force built up by the PETC activity. Moreover, the stromal concentration of NADPH is dynamic.

\begin{figure}
  \begin{minipage}[c]{0.67\textwidth}
    \includegraphics[width=\textwidth]{mergedmodel_scheme.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.3\textwidth}
  \justify
    \caption{Schematic representation of the photosynthetic processes described by our \textit{merged} mathematical model. The reactions take place in two compartments: \textbf{lumen}, where the four protein supercomplexes are embedded driving the electron transport in two modes, linear and cyclic; and the \textbf{stroma}, compartment of the C3 photosynthetic carbon fixation. The cytosol defines the system boundary. In colour (green and blue) we have highlighted the reactions linking the two submodels: the production and consumption of ATP and NADPH.} \label{fig:scheme}
  \end{minipage}
\end{figure}

\subsection{Model compartments and units}
The merged models were developed for different organisms (\cite{Ebenhoh2014} for \textit{C. reinhardtii}, \cite{Matuszynska2016} for \textit{A. thaliana} and \cite{Poolman2000, Pettersson1988} based on data for isolated spinach chloroplasts) and express the concentrations and rates in different units. 
To keep the original structure of the models, but provide consistency, we have kept the original units for each of the compartments and used a conversion factor ($p_\textrm{convf}$, see Supplement) to convert quantities where needed. 
Thus, the concentrations of proteins and pool sizes inside the lumen are expressed as in previous models of the electron transport~\cite{Ebenhoh2014,Matuszynska2016} in mmol(mol Chl)$^{-1}$,  and the first order rates in mmol(mol Chl)$^{-1}$s$^{-1}$. Concentrations of metabolites and pools inside the stroma are expressed in mM, as in \cite{Pettersson1988,Poolman2000}. 
To convert the concentration of ATP produced through the electron transport chain activity, expressed in mmol(mol Chl)$^{-1}$, to mM, used to express concentrations in the stroma, we made several assumptions, as in our previous models of photosynthesis~\cite{ebenhoh2011minimal,Ebenhoh2014,Matuszynska2016}, which were originally derived from Laisk \textit{et al.}~\cite{Laisk2006}: i) chlorophyll content is assumed to be fixed and equal to 350 · 10$^{-6}$ mol per m$^2$ thylakoid membrane; ii) the volume of thylakoid stroma and lumen are 0.0112 l m$^{-2}$ and 0.0014 l m$^{-2}$, respectively. Thus, 1 mmol(mol Chl)$^{-1}$ corresponds to 2.5 · 10$^{-4}$M in the lumen and 3.2 · 10$^{-5}$M in the stroma. 

\subsection{Computational analysis}
The model has been implemented using the \texttt{modelbase} software, a console-based application written in Python, developed by us earlier this year~\cite{Ebenhoh2018}. Stoichiometry and parameters are provided in the Supplement and as a text file, to be found on our GitHub repository (\url{www.github.com/QTB-HHU/photosynthesismodel}). Moreover, we provide a Jupyter Notebook, that allows the user to repeat all the simulations leading to the production of the figures presented in this manuscript.

\subsection{Reliability of the model}
We have assembled the model of photosynthesis adapting previously validated and published mathematical models of two interdependent processes. 
We have used the same parameters as reported in the previous work and did not perform any further parameter fits (the full list of parameters in Supplement Tables~\ref{tab:pools:par}-\ref{tab:phys:par}). 
We have monitored the evolution of several critical parameters to evaluate physiological plausibility of our computational results, including lumenal pH (kept under moderate light around 6), RuBisCO rate (in the order of magnitude of measured values) and the redox state of the PQ pool, used as an estimate of the overall redox state. Moreover, systematic steady state analysis of the model under different light conditions lead to plausible concentrations of CBB cycle intermediates and fluxes, as reported in the literature~\cite{Pettersson1988}.



