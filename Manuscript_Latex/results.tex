We employed our merged model of photosynthesis and carbon fixation to perform a systematic supply-demand analysis of the coupled system.
First, we have integrated the system for various constant light intensities until it reached steady state. 
Examples are provided in the Supplement (Figure~\ref{fig:si:steadystate}).
We observed reasonable stationary values of intermediates and fluxes for most of the light intensities. 
However, under very low light intensities (below 5 ~\PFD), the phosphorylated CBB cycle intermediates dropped to zero, and ATP assumed the maximal concentration equalling the total pool of adenosine phosphates. 
Depending on the initial conditions, either a non-functioning state, characterised by zero carbon fixation rate, or a functioning state, characterised by a positive stationary flux, was reached.
This observation of bistability constituted the starting point of our analysis of the tight supply-demand relationship.

In order to analyse this behaviour in more detail, we performed time course simulations, in which the light was dynamically switched from constant sufficient light (between 20 and 300~\PFD), to a "dark phase" of 200 s duration with a light intensity of 5~\PFD, back to high light, and observed the dynamics of the model variables. 
In Figure~\ref{fig:LDL} we display the dynamics of the internal orthophosphate concentration, the sum of all three triose phosphate transporter (TPT) export rates and the RuBisCO rate (from top to bottom respectively) during such light-dark-light simulation.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{Fig2.png}
	\caption{Simulations of light-dark-light transitions for different light intensities, ranging from 20-200~\PFD. Shown are the dynamics of internal orthophosphate concentration, triose phosphate transporter (TPT) export and carbon fixation rates. The simulated time-courses are shown from 200s, when the system has reached a stationary state. From 300-500s (grey area), the external light has been set to 5~\PFD. The figure illustrates that for low light intensities the CBB cycle fails to restart in the second light period.}
    \label{fig:LDL}
\end{figure}
In agreement with the steady-state simulations, higher light intensities result in a higher overall flux during the initial light phase. 
Higher carbon fixation and export fluxes are accompanied by lower orthophsophate concentrations, which reflect higher levels of CBB cycle intermediates.
In the dark phase, the non-functional state with zero carbon flux is approached.
While rates decrease, orthophosphate increases, reflecting a depletion of the CBB intermediate pools. 
In the second light phase, only the simulated transitions to light intensities of 150 and 200~\PFD could recover a functional state under the chosen conditions. 
For lower light intensities, apparently the CBB intermediate pool was depleted to a level, at which re-illumination fails to recover the CBB cycle activity. 
Obviously, this behaviour disagrees with everyday observations in nature (plant leaves recover from dark periods also under low light intensities). 
Nevertheless, the model is useful to generate novel insight. First, it illustrates that there exists a critical threshold of intermediate concentrations. 
If levels drop below this threshold, the cycle cannot be re-activated. 
Second, it explains the mechanisms leading to intermediate depletion. Under low light conditions insufficient energy supply results in reduced activity of ATP and NADPH dependent reactions in the carbon fixation cycle, leading to a reduced regeneration rate of Ribulose 1,5-bisphosphate (RuBP) from Ribulose-5-Phosphate (Ru5P). 
Simultaneously, the reversible (ATP independent) reactions remain active. 
Since triose phosphates are products of reversible reactions, these continue to be exchanged via the TPT export reactions with free phosphate, which leads to a depletion of the CBB cycle intermediates and a concomitant increase of the orthophosphate pool.

\begin{figure}[h!]
\begin{center}
	\includegraphics[width=0.8\linewidth]{bistability_svg}
	\caption{Simulations in light intensity of 500 ~\PFD for different initial concentrations of RU5P, ranging from 0.35 to 0.5 mM. The RU5P abundance is shown after 10s, when the system is approximately equilibrated. The dashed line displays the critical concentration for sufficient cyclic activity after equilibrating. The figure displays that initial RU5P concentrations below 0.44 mM result in a loss of RU5P abundance.}
    \label{fig:initRu5P}
    \end{center}
\end{figure}

Clearly, the model is missing important mechanisms that prevent such a functional failure. 
In particular, we are interested in how a \textbf{stand-by mode} can be realised, in which intermediate levels are maintained above the critical threshold, while at the same time  the resources required to do so, are minimised.
A possible strategy to prevent the collapse of the carbon fixation cycle is to resupply important intermediates. 
One biochemical process in plants that is known to produce Ru5P is the oxidative phase of the Pentose phosphate pathway (PPP), in which one Glucose-6-phosphate molecule is oxidised and decarboxylated to Ru5P, while producing NADPH and CO$_2$ \cite{Kruger2003}.
In order to estimate critical intermediate levels required to prevent the collapse of the carbon fixation cycle, we performed simulations under sufficient light (500~\PFD), with different initial conditions. 
The initial concentrations of all carbon fixation intermediates are set to zero, except for Ru5P. 
The simulated Ru5P concentration, depicted in Figure~\ref{fig:initRu5P}, displays a characteristic dynamics. 
In the first seconds, the CBB cycle intermediates are equilibrated by the fast reversible reactions. 
If this concentration remains above the critical threshold of approximately 2.5~$\mu$M, the cycle reaches a functional state, if it falls below, it will collapse.  
Interestingly, the threshold concentration is rather independent of the light intensity (see Figure~\ref{fig:si:bistability}).

\newpage
To simulate a simple mechanism implementing a stand-by mode, which maintains sufficient CBB cycle intermediate levels, we introduced a trivial conceptual reaction, exchanging inorganic phosphate with Ru5P.
Figure~\ref{fig:Ru5Pinflux} displays simulated steady state values of the relative stromal ATP concentrations, Ru5P concentrations and lumenal pH in insufficient light conditions (5~\PFD) as a function of the  Ru5P influx.
Again, a clear threshold behaviour can be observed. If the Ru5P influx exceeds approximately 4~$\mu$M/s, not only CBB intermediates  assume non-zero concentrations, but also the lumenal pH reaches realistic and non-lethal levels. 
\begin{figure}[hb!]
  \begin{center}
  \includegraphics[width=0.8\linewidth]{oxPPP_main}
  \justify
	\caption{Steady state simulations in low light intensity of 5 ~\PFD and systematically increasing influxes of Ru5P from 0. to 0.08 mM/s. The figure displays normalised ATP abundance, Ru5P concentration and lumenal pH.}
        \label{fig:Ru5Pinflux}
  \end{center}
\end{figure}
As expected, increased Ru5P influx results in its increased stationary concentrations, which is accompanied by an increased flux through RuBisCO and the TPT exporter (Figure~\ref{fig:si:Ru5Pinflux}), 
indicating a higher stand-by flux, and therefore, a higher requirement of resources to maintain this mode.
%Interestingly, increasing the Ru5P influx does not significantly affect starch production flux.

These results suggest that a constant flux providing Ru5P in the dark with a rate just above the critical threshold of 4~$\mu$M/s should maintain intermediate CBB levels sufficiently high, while at the same time minimise the required investment. 
Indeed, with a constant supply of Ru5P with 5~$\mu$M/s, the system can be restarted and reaches a functional stationary state after a prolonged dark period (see Figure~\ref{fig:si:standby}). 
Per carbon, this rate translates to 25-30~$\mu$M carbon/s, depending whether the pentoses are directly imported or derived from hexoses. 
Comparing this to stationary carbon fixation in the light of 0.1-1 mM/s (for light intensities between 20 and 200~\PFD, see Figure~\ref{fig:LDL} and Figure~\ref{fig:si:steadystate}) shows that resupply under these conditions would consume a considerable fraction of the previously fixed carbon. 
This calculation demonstrates the importance of down-regulating the CBB cycle in dark conditions for a positive carbon fixation balance over a day/night cycle.
Indeed, key enzymes in the carbon fixation cycle are known to be regulated by the pH and the redox state of the chloroplast stroma. 
For example, RuBisCO activity is controlled by proton levels and magnesium ions~\cite{Andersson2008, Tapia2000}.
Fructose-1,6-biphosphatase, Seduheptulose-1,7-biphosphatase and Phosphoribulokinase are all controlled by the redox state through the thioredoxin-ferredoxin system, and also by pH \cite{Chiadmi1999, Raines2000,Raines2003}.
Furthermore, Hendriks \textit{et al.}\ showed the light dependency of the ADP-glucose pyrophosphorylase~\cite{hendriks2003adp}, which is part of the lumped reaction $v_\mathrm{Starch}$ in our model.
All these mechanisms will lead to a considerable reduction of the required stand-by flux of the CBB cycle, but are not yet included in our simple merged model.
% [FIXME: could we estimate to which fraction the enzymes need to be reduced, so that we spend at most 5\% or so of the fixed carbon to maintain the cycle's integrity??]

In the original formulation of our model without constant Ru5P supply or light-dependent regulation of CBB enzymes, low light intensities lead to a rapid collapse of the cycle.
In sufficient light, however, ATP levels are very high and carbon fixation rates are already saturated in moderate light conditions (Figure~\ref{fig:LDL} and Figure~\ref{fig:si:steadystate}).
These findings indicate that the sets of parameters for the carbon fixation enzymes and the light reactions, derived from the respective original publications, might not be suitably adapted when employed in a merged, cooperating, system.
This is not surprising considering that they originate from completely different systems and conditions.

In order to systematically investigate the supply-demand behaviour of the coupled system in different light conditions,
we introduce a 'regulation factor' $f_\mathrm{CBB}$ of the CBB cycle, by which all $V_\mathrm{max}$-values of the light-regulated enzymes (see above) are multiplied.
This allows for a systematic variation of the energy demand by simulating accelerated or decelerated carbon fixation activity. 
Performing this variation under different light conditions gives insight into the synchronisation of ATP and NADPH production and consumption rates, and thus enables a more profound analysis the supply-demand regulation of photosynthesis~\cite{H.BrandesFWLarimer1996,Chiadmi1999,Raines2000}.
For the following steady-state analysis, the conceptual Ru5P influx reaction is not included.
\begin{figure*}[h]
\begin{center}
        \begin{subfigure}[b]{0.475\textwidth}
            \includegraphics[width=\textwidth]{ATPsurface}
            \caption[]%
            {{\small relative ATP abundance}}    
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.475\textwidth}  
            \includegraphics[width=\textwidth]{TPTsurface}
            \caption[]%
            {{\small TPT export flux}}    
        \end{subfigure}
        \vskip\baselineskip
        \begin{subfigure}[b]{0.475\textwidth}   
            \includegraphics[width=\textwidth]{Starchsurface}
            \caption[]%
            {{\small Starch production flux}}    
        \end{subfigure}
        \quad
        \begin{subfigure}[b]{0.475\textwidth}   
            \includegraphics[width=\textwidth]{Protonsurface}
            \caption[]%
            {{\small Lumenal pH}}    
        \end{subfigure}
        \caption{3D display of the steady state analysis of the system under varying light intensities (x-axis) and carbon fixation velocities (y-axis). On the z-axis: (a) the relative ATP abundance, (b) TPT export flux, (c) starch production rate, and (d) lumenal pH are displayed.}
\label{fig:surfacePlots}
\end{center}
    \end{figure*}
Figure~\ref{fig:surfacePlots} displays stationary values of key model variables for different light intensities and regulation factors.
In agreement with the observations presented above that very low light intensities lead to a collapse of the cycle, ATP concentrations (Figure~\ref{fig:surfacePlots}a) are maximal (zero ADP), triose phosphate export (Figure~\ref{fig:surfacePlots}b) and starch production (Figure~\ref{fig:surfacePlots}c) are zero, and the lumenal pH (Figure~\ref{fig:surfacePlots}d) is very low (around 4). 
The latter is readily explained by the fact that the pH gradient built up by the low light cannot be reduced by the ATPase, which lacks the substrate ADP.
Further, it becomes clear that the regulation factor of $f_\mathrm{CBB}=1$, corresponding to the original parameters, is far from optimal. 
The ATP:ADP ratio remains very high, and TPT export and starch production rates are well below their optimum, regardless of the light intensities. 
The stationary lumenal pH further illustrates that parameters are not ideally adjusted. 
Not only for very low light, but also for moderate to high light conditions (above 300~\PFD) the lumen is dramatically acidic, indicating a mismatch in production and consumption processes.
Increasing the regulation factor to values $f_\mathrm{CBB}\approx 4$ leads to a dramatic improvement of the performance of the system. 
The ATP:ADP ratio assumes realistic and healthy values around one, triose phosphate export approximately doubles, and starch production increases by one order of magnitude compared to the original parameter values. 
Concomitantly, the lumenal pH remains moderate (>5.8, as suggested in~\cite{Kramer1999}).

% Figure~\ref{fig:surfacePlots}a displays the (normalised) steady state ATP concentration under different light intensities and for different regulation factors. 
% Very low intensities always result in total occupancy of the AP pool by ATP, and therefore a collapsed CBB cycle. 
% Low regulation factors result in a minor decrease in ATP abundance in moderate light intensities, and a subsequent increase in high light intensities. In contrast, high regulation factors result in a strong decrease in ATP abundance in moderate and high light intensities. 
% The stationary triose phosphate export flux is displayed in Figure~\ref{fig:surfacePlots}b. 
% Very low light intensities always result in zero export activity, while increasing light intensities gradually increase the TPT export flux. 
% Here, increasing the regulation factor result in increased export flux only for small regulation factors. An increase above $f_\mathrm{CBB}\approx 4$ does not further increase the TPT export flux.
% The stationary starch production is shown in Figure~\ref{fig:surfacePlots}c. It gradually increases with increasing light intensity. It also increases with increased regulation factors, but this increase is drastically reduced for values above $f_\mathrm{CBB}\approx 4$. Figure~\ref{fig:surfacePlots}d shows the lumenal pH in steady state. The lumenal pH is low (pH 4-5) in very low light intensites. With low regulation factors, low light intensities increase the pH to 6, and increasing light intensities decrease the pH up to 3-3.5 . Increasing regulation factors display similar behaviour, with a pH decrease starting in higher light intensities. Very high regulation factors do not exhibit a decrease in pH in increasing light intensities.
%\\
%\newline
%\noindent 
% Quantitative analysis of the systems supply-demand behaviour can be performed by calculating flux control coefficients. Despite the reduced complexity of the system, a calculation of control coefficients of all $V_{max}$ parameters for all reactions can be confusing and is hardly overviewable. Furthermore, different light intensities and carbon fixation cycle regulations result in different distributions of control coefficients. Therefore, in order to calculate the different system control of the supply and demand reactions of the system, we calculated the control coefficients for all demand and supply reaction associated $V_\mathrm{max}$-values. We consider demand reactions as all reactions in the carbon fixation cycle, and supply reactions as all reactions in the PETC. \\ \newline

Quantitative analysis of the supply-demand behaviour of the system can be performed by calculating flux control coefficients~\cite{Kacser1973, Heinrich1974}. 
To investigate the relative overall flux control of supply and demand reactions, we first divide the set of all reactions in the model (R) 
into two non-overlapping sets S and D. S represents the supply set containing all PETC reactions and $\mathrm{D}$ represents the demand reaction set including all CBB cycle reactions. %(Eq.~\ref{eq:set2}).
% \begin{align}
% \mathrm{R} = \mathrm{S} \cup \mathrm{D} \\ \label{eq:set1}
% \mathrm{S} \cap \mathrm{D}  = \emptyset %\label{eq:set2}
% \end{align} %how to tag/label multiple lines?
We define the the overall control of supply ($C_{\mathrm{Supply}}$) and demand ($C_{\mathrm{Demand}}$) reactions as the sum of the absolute values of all control coefficients of reactions from $\mathrm{S}$ and $\mathrm{D}$, respectively, on the steady-state flux through the RuBisCO reaction,
\begin{align}
C_\mathrm{Demand} = \sum_{k \in \mathrm{D}}|C_{k}^{J}| \\
C_\mathrm{Supply} = \sum_{k \in \mathrm{S}}|C_{k}^{J}|.
\end{align}

Figure~\ref{fig:fcc} displays the normalized overall control of demand reactions $C_\mathrm{Demand}/(C_\mathrm{Demand}+C_\mathrm{Supply})$, in dependence on different light intensities and carbon fixation regulation factors. 
Low light intensities and fast carbon fixation reactions shift the overall flux control to the supply reactions. 
This can readily be explained because under these conditions (low light and fast CBB enzymes) energy and redox provision by the light reactions are the limiting factor. 
Interestingly, PSII and PSI contribute strongest to the overall
flux control on the supply side (Figure~\ref{fig:si:fcc50}).
Conversely, high light intensities and slow carbon fixation reactions shift the overall flux control to the demand side, because under these conditions, the system is energetically saturated, and the bottleneck is in the CBB cycle consuming the energy and redox equivalents. 
Noteworthy, it is the SBPase reaction that exhibits the highest overall flux control (Figure~\ref{fig:si:fcc300}), while RuBisCO has only minor control.

\begin{figure}[t]
  \begin{minipage}[c]{0.67\textwidth}
	\includegraphics[width=\linewidth]{FCC.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.3\textwidth}
  \justify
    \caption{Normalized overall control of the demand reactions ($C_\mathrm{Demand}$) under different light intensities (x-axis) and CBB cycle activities (y-axis). The results show how the control shifts from the demand reactions under high light conditions, but low CBB activity, to the supply, under low light conditions but faster CBB cycle.} \label{fig:fcc}
  \end{minipage}
\end{figure}
%Increasing light in low $f_\mathrm{CBB}$ saturates the system in light and therefore increases the flux control of demand reactions. Higher $f_\mathrm{CBB}$ result in a similar behaviour, stretched to higher light intensities. This can be explained by the dependency of light saturation on an energetic saturation of the demand reactions. In low $f_\mathrm{CBB}$ values this energetic saturation is reached in lower light intensities than in higher $f_\mathrm{CBB}$ values.