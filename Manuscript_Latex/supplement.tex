\noindent We are providing the mathematical description of the model of photosynthesis implemented in Python using the \texttt{modelbase} software developed in our Lab \cite{Ebenhoh2018}. The code can be downloaded from the GitHub repository (\url{https://github.com/QTB-HHU/photosynthesismodel}) and is accompanied by a Jupyter Notebook that allows user to easily repeat the simulations included in the main text of the "Balancing energy supply during photosynthesis - a~theoretical perspective" article.

\subsection{Mathematical Model}
The mathematical model of photosynthesis is a result of merging previously developed mathematical models of the photosynthetic electron transport chain \cite{Ebenhoh2014}, complemented with the four-state description of the non-photochemical quenching \cite{Matuszynska2016}, with the Poolman~\cite{Poolman2000} implementation of the model of C3 photosynthesis carbon fixation \cite{Pettersson1988}.

\subsubsection{Stoichiometry}
The system of equations comprises a set of 24 coupled ordinary differential equations and 34 reaction rates. 
The first 9 differential equations describe dynamics of the PETC and follow the change of the
oxidised fraction of the plastoquinone pool (Eq.~\ref{eq:st:pqh2}), 
oxidised fraction of the plastocyanin pool (Eq.~\ref{eq:st:pc}), 
oxidised fraction of the ferredoxin pool (Eq.~\ref{eq:st:fd}), 
stromal concentration of ATP (Eq.~\ref{eq:st:atp}), 
stromal concentration of NADPH (Eq.~\ref{eq:st:nadph}), 
proton concentration in the lumen (Eq.~\ref{eq:st:h}), 
phosphorylated fraction of light harvesting complexes (Eq.~\ref{eq:st:ant}),
the fraction of non-protonated PsbS protein (Eq.~\ref{eq:prot}), and
the fraction of violaxanthin in the total pool of xanthophylls (Eq.~\ref{eq:deepox}):

\begin{empheq}[left=\empheqlbrace]{align}
&\frac{d\mathrm{[PQ]}}{dt} =
-v_\mathrm{PSII} + v_\mathrm{b6f} - v_\mathrm{FQR} + v_\mathrm{PTOX} - v_\mathrm{NDH}, \label{eq:st:pqh2}\\
&\frac{d\mathrm{[PC]}}{dt} =
-2 v_\mathrm{b6f} + v_\mathrm{PSI},\label{eq:st:pc}\\
&\frac{d\mathrm{[Fd]}}{dt} =
-v_\mathrm{\mathrm{[PSI]}} + 2 v_\mathrm{FNR} + 2 v_\mathrm{FQR},\label{eq:st:fd}\\
&\frac{d\mathrm{[ATP]}}{dt} =
v_\mathrm{ATPsynthase} \cdot p_\mathrm{convf} - v_\mathrm{vPGA\_kinase} - v_{13} - v_\mathrm{Starch},\label{eq:st:atp}\\
&\frac{d\mathrm{[NADPH]}}{dt} =
v_\mathrm{FNR}\cdot p_\mathrm{convf} - v_\mathrm{vBPGA\_dehydrogenase},\label{eq:st:nadph}\\
&\frac{d\mathrm{[H]}}{dt} =
\left(2 v_\mathrm{PSII} + 4 v_\mathrm{b6f} -\frac{14}{3} v_\mathrm{ATPsynthase} - v_\mathrm{leak}\right)\cdot\frac{1}{b_H},\label{eq:st:h}\\
&\frac{d\mathrm{[LHC]}}{dt} =
v_\mathrm{Stt7} - v_\mathrm{Pph1},\label{eq:st:ant}\\
&\frac{d\mathrm{[Psbs]}}{dt} =
-v_\mathrm{LHCprotonation} + v_\mathrm{LHCdeprotonation}, \label{eq:prot}\\
&\frac{d\mathrm{[Vx]}}{dt} =
-v_\mathrm{Deepox} + v_\mathrm{Epox}. \label{eq:deepox}
\end{empheq}

The next 15 differential equations govern the temporal evolution of the CBB cycle intermediates. The equations originate from the Pettersson and Ryde-Petterson model~\cite{Pettersson1988}:
\begin{empheq}[left=\empheqlbrace]{align}
&\frac{d\mathrm{[PGA]}}{dt} = 2 v_\mathrm{RuBisCO} - v_\mathrm{PGA\_kinase} - v_\mathrm{pga}, \\
&\frac{d\mathrm{[BPGA]}}{dt} = v_\mathrm{PGA_kinase} - v_\mathrm{BPGA\_dehydrogenase}, \\
&\frac{d\mathrm{[GAP]}}{dt} = v_\mathrm{BPGA\_dehydrogenase} - v_\mathrm{TPI} - v_\mathrm{Aldolase} - v_\mathrm{F6P\_Transketolase} - v_{10} - v_\mathrm{gap}, \\
&\frac{d\mathrm{[DHAP]}}{dt} = v_\mathrm{TPI} - v_\mathrm{Aldolase} - v_8 - v_\mathrm{dhap}, \\
&\frac{d\mathrm{[FBP]}}{dt} = v_\mathrm{Aldolase} - v_\mathrm{FBPase}, \\
&\frac{d\mathrm{[F6P]}}{dt} = v_\mathrm{FBPase} - v_\mathrm{F6P\_Transketolase} - v_{14}, \\
&\frac{d\mathrm{[G6P]}}{dt} = v_{14} - v_\mathrm{Phosphoglucomutase}, \\
&\frac{d\mathrm{[G1P]}}{dt} = v_\mathrm{Phosphoglucomutase} - v_\mathrm{Starch}, \\
&\frac{d\mathrm{[SBP]}}{dt} = v_8 - v_9, \\
&\frac{d\mathrm{[S7P]}}{dt} = v_9 - v_{10}, \\
&\frac{d\mathrm{[E4P]}}{dt} = v_\mathrm{F6P\_Transketolase} - v_8, \\
&\frac{d\mathrm{[X5P]}}{dt} = v_\mathrm{F6P\_Transketolase} + v_{10} - v_{12}, \\
&\frac{d\mathrm{[R5P]}}{dt} = v_{10} - v_{11}, \\
&\frac{d\mathrm{[RUBP]}}{dt} = v_{13} - v_\mathrm{RuBisCO}, \\
&\frac{d\mathrm{[RU5P]}}{dt} = v_{11} + v_{12} + v_\mathrm{oxPPP}- v_{13}.
\end{empheq}

Moreover, using the functionality of \texttt{modelbase}~\cite{Ebenhoh2018}, we have incorporated seven algebraic expressions from which we have derived seven dependent variables:
the reduced fraction of PQ (Eq.~\ref{eq:pqcons}),
the reduced fraction of PC (Eq.~\ref{eq:pccons}),
the reduced fraction of Fd (Eq.~\ref{eq:fdcons}),
the stromal concentration of ADP (Eq.~\ref{eq:apcons}),
the stromal concentration of NADP (Eq.~\ref{eq:nadpcons}),
the stromal concentration of orthophosphate (Eq.~\ref{eq:picons}),
and the inhibition factor of the triose phosphate translocators (Eq.~\ref{eq:ncons}):

\begin{empheq}{align}
\mathrm{[PQH_2]} &= \pqtot - \mathrm{[PQ]} \label{eq:pqcons} \\
\mathrm{[PC^-]} &= \pctot - \mathrm{[PC]} \label{eq:pccons}\\
\mathrm{[Fd^-]} &= \fdtot - \mathrm{[Fd]} \label{eq:fdcons}\\
\mathrm{[ADP]} &= \aptot - \mathrm{[ATP]} \label{eq:apcons}\\
\mathrm{[NADP^+]} &= \nadptot - \mathrm{[NADPH]} \label{eq:nadpcons}\\
\mathrm{[P_i]} &= \mathrm{P^{tot}} - (\mathrm{[PGA]} + 2 \cdot \mathrm{[BPGA]} + \mathrm{[GAP]} + \mathrm{[DHAP]} + 2 \cdot \mathrm{[FBP]} + \mathrm{[F6P]} + \nonumber \label{eq:picons}\\
&+ \mathrm{[G6P]} + \mathrm{[G1P]} + 2 \cdot \mathrm{[SBP]} + \mathrm{[S7P]} + \mathrm{[E4P]} + \mathrm{[X5P]} + \mathrm{[R5P]} + 2 \cdot \mathrm{[RUBP]} + \mathrm{[RU5P]} + \mathrm{[ATP]}) \\
N &= 1 + \left(1 +\frac{K_\mathrm{Pext}}{\mathrm{[P_{ext}]}}\right) \cdot \left(\frac{\mathrm{[P_{i}]}}{K_\mathrm{pi}} + \frac{\mathrm{[PGA]}}{K_\mathrm{pga}} + \frac{\mathrm{[GAP]}}{K_\mathrm{gap}} + \frac{\mathrm{[DHAP]}}{K_\mathrm{dhap}}\right)
\label{eq:ncons}
\end{empheq}
where P$_\mathrm{ext}$ denotes external orthophosphate and $K_\mathrm{x}$ represents the apparent dissociation constant for the formed complex between.

Additionally, the overall quencher activity can be derived using the following equation
\begin{equation}
Q = \gamma_0 \cdot (1-\frac{\mathrm{[Zx]}}{\mathrm{[Zx]}+K_\mathrm{{ZSat}}})\cdot \mathrm{[PsbS]}+ \gamma_1\cdot(1-\frac{\mathrm{[Zx]}}{\mathrm{[Zx]}+K_\mathrm{{ZSat}}})\cdot \mathrm{[PsbS^p]} + \gamma_2\cdot \frac{\mathrm{[Zx]}}{\mathrm{[Zx]}+K_\mathrm{{ZSat}}}\cdot \mathrm{[PsbS^p]} + \gamma_3\cdot \frac{\mathrm{[Zx]}}{\mathrm{[Zx]}+K_\mathrm{{ZSat}}}\cdot \mathrm{[PsbS]},
\end{equation}
where $\mathrm{[Zx]}$ is the concentration of deepoxidised xantophylls ($[\mathrm{X^{tot}]} = \mathrm{[Zx]}+\mathrm{[Vx]}$), $\mathrm{[PsbS^p]}$ is the concentration of protonated $\mathrm{PsbS}$ protein and the parameters are described in Table~\ref{tab:npq:par}.

\subsubsection{Reaction rates}
The rate equations of all, except three reaction rates linking the submodels, have been kept as in the original works~\cite{Pettersson1988,Poolman2000,Ebenhoh2014,Matuszynska2016}. We have substituted the two consuming reactions from \cite{Ebenhoh2014} with the whole CBB cycle model and the one ATP synthase reaction of the \cite{Pettersson1988} with the ATP synthase reaction from \cite{Ebenhoh2014}. Nevertheless, to allow for an easy and self-sufficient read and to foster faster reproducibility of this work, we are providing below the full set of the reaction rates used for this model.

\textbf{The quasi-steady state approximation used to calculate the rate of photosystem II (PSII)}
\begin{equation} 
\begin{aligned}
- \left(k_\mathrm{LII} + \frac{k_\mathrm{PQred}}{K_\mathrm{eq,QAPQ}}\cdot \mathrm{[PQH_2]} \right) \cdot B_0 + (k_H \cdot Q+k_F) \cdot B_1 + k_\mathrm{PQred} \cdot \mathrm{[PQ]} \cdot B_3 = 0,\\
k_\mathrm{LII} \cdot B_0 - (k_H \cdot Q + k_F + k_P) \cdot B_1 = 0,\\
k_\mathrm{LII} \cdot B_2 - (k_H \cdot Q + k_F) \cdot B_3 = 0,\\
B_0 + B_1 + B_2 + B_3 = \psiitot ,\\
\end{aligned}
\end{equation}
where $k_\mathrm{LII}$ is the light activation rate of PSII and $B_i$, where $i\in(0,1,2,3)$, is a temporary state of photosystem II, relating light harvesting capability with the occupation of reaction centres. 

\textbf{The quasi-steady state approximation used to calculate the rate of photosystem I (PSI)}
\begin{equation} 
\begin{aligned}
 - \left(k_\mathrm{LI} + \frac{k_\mathrm{PCox}}{K_\mathrm{eq,PCP700}}\cdot \mathrm{[C]}\right) \cdot Y_0 + k_\mathrm{PCox}\cdot \mathrm{[PC^-]}\cdot Y_2 = 0, \\
k_\mathrm{LI}Y_0 - k_\mathrm{Fdred}\cdot \mathrm{[F]}\cdot Y_1 + \frac{k_\mathrm{Fdred}}{K_\mathrm{eq,P700Fd}}\cdot \mathrm{[Fd^-]} \cdot Y_2  = 0, \\
Y_0 + Y_1 + Y_2 = \psitot,
\end{aligned}
\end{equation}
where $k_\mathrm{LI}$ is the light activation rate of PSI (determined by the total light intensity and the relative cross-section of PSI) and Y$_i$, where $i\in(0,1,2)$ corresponds to one of the states of the PSI.

\textbf{The reactions of the PETC}
\begin{empheq}{align}
& v_{\mathrm{PSII}} = k_2 \cdot 0.5 \cdot B_1, \\
& v_{\mathrm{PSI}} = k_\mathrm{LI}\cdot Y_0, \\
& v_\mathrm{b6f} = \max\left(k_\mathrm{b6f} \cdot \left( \mathrm{[PQ]}\cdot \mathrm{[PC^-]}^2 - \frac{\mathrm{[PQH_2]}\cdot \mathrm{[PC]}^2}{K_\mathrm{eq,b6f}(H)}\right), v^\mathrm{min}_\mathrm{b6f}\right), \\
& v_\mathrm{ATPsynthase} = k_\mathrm{ATPsynthase}\cdot \left(\mathrm{[ADP]} - \frac{\mathrm{[ATP]}}{K_\mathrm{eq,ATPsynthase}(H)} \right),\\
& v_\mathrm{FNR} = V^\mathrm{max}_\mathrm{FNR} \cdot
  \frac{{f^-}^2 \cdot n^+ - (f^2 \cdot n)/K_\mathrm{eq,FNR}}
    {(1+f^-+{f^-}^2)\cdot (1+n^+) + (1+f+f^2)\cdot (1+n^+) -1}, \\
& v_\mathrm{FQR} = k_\mathrm{FQR}\cdot \mathrm{[F]}^2\cdot \mathrm{[PQH_2]}, \mathrm{~where:}  \\
&f = \frac{\mathrm{[FQ]}}{K_\mathrm{M,F}}, f^- = \frac{\mathrm{[FQ^-]}}{K_\mathrm{M,F}}, n^+ = \frac{\mathrm{[NADP^+]}}{K_\mathrm{M,N}}, n = \frac{\mathrm{[NADPH]}}{K_\mathrm{M,N}} \nonumber \\
& v_\mathrm{NDH} = k_\mathrm{NDH}\cdot \mathrm{[PQH_2]}, \\
& v_\mathrm{Leak} = k_\mathrm{leak} \cdot (\mathrm{[H]} - \mathrm{[H_{stroma}]}),\\ 
& v_\mathrm{PTOX} = k_\mathrm{PTOX}\cdot \mathrm{O}_2^\mathrm{ext}\cdot \mathrm{[PQ]}, \\
& v_\mathrm{Stt7} = k_\mathrm{Stt7}\cdot\left(
\frac{1}{1+\left(\frac{\mathrm{[PQH_2]}/\pqtot}{K_\mathrm{M,ST}}\right)^{n_\mathrm{ST}}}
\right)\cdot (1-\mathrm{[LHC]}),\\
& v_\mathrm{Pph1} = k_\mathrm{Pph1}\cdot \mathrm{[LHC]},\\
& v_\mathrm{Deepox} = k_\mathrm{DeepoxV} \cdot \frac{H^\mathrm{nH_X}}{H^\mathrm{nH_X} pH_{\mathrm{inv}}(K_\mathrm{phSat})^\mathrm{nH_X}} \cdot \mathrm{[Vx]} \\
& v_\mathrm{Epox} = - k_\mathrm{EpoxZ} \cdot \mathrm{[Zx]},\\
& v_\mathrm{Lhcprotonation} = k_\mathrm{ProtonationL} \cdot \frac{H^\mathrm{nH_L}} {H^\mathrm{nH_L} + pH_{\mathrm{inv}}(K_\mathrm{phSatLHC})^\mathrm{nH_L}} \cdot  \mathrm{[PsbS]} \\
& v_\mathrm{Lhcdeprotonation}= - k_\mathrm{Deprotonation} \cdot \mathrm{[PsbS^p]}.
\end{empheq}

\textbf{The reactions of the CBB Cycle}
\begin{empheq}{align}
& v_\mathrm{RuBisCO} = \frac{V_1\cdot\mathrm{[RUBP]}}{\mathrm{[RUBP]} + K_\mathrm{m1} \cdot \left(1 + \frac{\mathrm{[PGA]}}{K_\mathrm{i11}} + \frac{\mathrm{[FBP]}}{K_\mathrm{i12}} + \frac{\mathrm{[SBP]}}{K_\mathrm{i13}} + \frac{\mathrm{[P_{i}]}}{K_\mathrm{i14}} + \frac{\mathrm{[NADPH]}}{K_\mathrm{i15}}\right)}\\
& v_\mathrm{vFBPase} = \frac{V_6 \cdot\mathrm{[FBP]}}{\mathrm{[FBP]} + K_\mathrm{m6} \cdot \left(1 + \frac{\mathrm{[F6P]}}{K_\mathrm{i61}} + \frac{\mathrm{[P_{i}]}}{K_\mathrm{i62}}\right)}\\
& v_9 = \frac{V_9\cdot\mathrm{[SBP]}}{\mathrm{[SBP]} + K_\mathrm{m9} \cdot \left(1 + \frac{[P_{i}]}{K_\mathrm{i9}}\right)}\\
& v_\mathrm{13} = \frac{V_{13}\cdot\mathrm{[RU5P]}\cdot\mathrm{[ATP]}}{\left(\mathrm{[RU5P]} + K_\mathrm{m131} \cdot (1 + \frac{[PGA]}{K_\mathrm{i131}} + \frac{[RUBP]}{K_\mathrm{i132}} + \frac{\mathrm{[P_{i}]}}{K_\mathrm{i133}})\right) \cdot \left(\mathrm{[ATP]} \cdot (1 + \frac{[ADP]}{K_\mathrm{i134}}) + K_\mathrm{m132} \cdot (1 + \frac{\mathrm{[ADP]}}{K_\mathrm{i135}})\right)} \\
& v_\mathrm{pga} = \frac{V_{ex}\cdot\mathrm{[PGA]}}{N \cdot K_\mathrm{pga}} \\
& v_\mathrm{gap} = \frac{V_{ex}\cdot\mathrm{[GAP]}}{N \cdot K_\mathrm{gap}} \\
& v_\mathrm{DHAP} = \frac{V_{ex}\cdot\mathrm{[DHAP]}}{N \cdot K_\mathrm{dhap}} \\
& v_\mathrm{Starch} = \frac{V_{st}\cdot\mathrm{[G1P]}\cdot\mathrm{[ATP]}}{\left(\mathrm{[G1P]} + K_\mathrm{mst1}\right)\cdot\left((1 + \frac{\mathrm{[ADP]}}{K_\mathrm{ist}}) \cdot (\mathrm{[ATP]} + K_\mathrm{mst2}) + \frac{K_\mathrm{mst2}\mathrm{[P_{i}]}}{K_\mathrm{ast1}\mathrm{[PGA]} + K_\mathrm{ast2}\cdot \mathrm{[F6P]} + K_\mathrm{ast3}\cdot\mathrm{[FBP]}}\right)},
\end{empheq}

where the close-to-equilibrium approximations from the Pettersson and Ryde-Pettersson original model \cite{Pettersson1988} has been dropped in favour of Poolman implementation \cite{Poolman2000}, using fast rate constant ($k$):
\begin{empheq}{align}
& v_\mathrm{PGA\_{kinase}} = k \cdot (\mathrm{[ATP]} \cdot \mathrm{[PGA]}-\frac{1}{\mathrm{q_2}} \cdot \mathrm{[ADP]} \cdot \mathrm{[BPGA]})\\
& v_\mathrm{BPGA\_dehydrogenase} = k \cdot (\mathrm{[NADPH]} \cdot \mathrm{[BPGA]} \cdot \mathrm{H_{stroma}}-\frac{1}{\mathrm{q_3}} \cdot \mathrm{[GAP]} \cdot \mathrm{[NADP]} \cdot P)\\
& v_\mathrm{TPI} = k \cdot (\mathrm{[GAP]}-\frac{1}{\mathrm{q_4}} \cdot \mathrm{[DHAP]})\\
& v_\mathrm{Aldolase} = k \cdot (\mathrm{[GAP]} \cdot \mathrm{[DHAP]}-\frac{1}{\mathrm{q_5}}\cdot \mathrm{[FBP]})\\
& v_\mathrm{{F6P}_{Transketolase}} = k \cdot (\mathrm{[GAP]}\cdot \mathrm{[F6P]}-\frac{1}{\mathrm{q_7}}\cdot \mathrm{[X5P]}\cdot \mathrm{[E4P]})\\
& v_8 = k \cdot (\mathrm{[DHAP]} \cdot \mathrm{[E4P]} -\frac{1}{\mathrm{q_8}}\cdot \mathrm{[SBP]})\\
& v_{10} = k \cdot(\mathrm{[GAP]} \cdot \mathrm{[S7P]}-\frac{1}{\mathrm{q_{10}}} \cdot \mathrm{[X5P]}\cdot \mathrm{[R5P]})\\
& v_{11} = k \cdot(\mathrm{[R5P]}-\frac{1}{\mathrm{q_{11}}} \cdot \mathrm{[RU5P]})\\
& v_{12} = k \cdot(\mathrm{[X5P]}-\frac{1}{\mathrm{q_{12}}} \cdot \mathrm{[RU5P]})\\
& v_{14} = k \cdot(\mathrm{[F6P]}- \frac{1}{\mathrm{q_{14}}} \cdot \mathrm{[G6P]})\\
& v_{15} = k \cdot (\mathrm{[G6P]}-\frac{1}{\mathrm{q_{15}}} \cdot \mathrm{[G1P]}).
\end{empheq}

\subsubsection{Parameters}
The complete summary of parameters used in the model is included in Tables \ref{tab:pools:par}-\ref{tab:phys:par}.

\begin{table*}[h]
\caption{Pool sizes.}\label{tab:pools:par}
\footnotesize
\begin{center}
\begin{tabular}{lll}
\textbf{Parameter} & \textbf{Value} & \textbf{Description and reference} \\[0.5ex]
\hline\\[-1ex]
% pool sizes
\multicolumn{3}{c}{\textbf{Lumenal side [$\mathrm{mmol(mol\ Chl)^{-1}}$]}}\\[1ex]
\hline\\[-1ex]
$\psiitot$ & $2.5$ & PSII reaction centres. Unchanged from ~\cite{Ebenhoh2014}\\
$\psitot$ & $2.5$ & PSI reaction centres. Unchanged from ~\cite{Ebenhoh2014}\\
$\pqtot$ & $17.5$ & PQ + $\mathrm{PQH_2}$ . Unchanged from ~\cite{Ebenhoh2014}\\
$\pctot$ & $4$ & PC$^-$ + PC$^+$. Unchanged from ~\cite{Ebenhoh2014}\\
$\fdtot$ & $5$ & Fd$^-$ + Fd$^+$. Unchanged from ~\cite{Ebenhoh2014}\\ 
PsbS$^\textrm{tot}$ & 1 & relative pool of PsbS. Unchanged from ~\cite{Matuszynska2016}\\
X$^\textrm{tot}$ & 1 & relative pool of xantophylls (Vx+$\mathrm{Zx}$). Unchanged from ~\cite{Matuszynska2016}\\
$\exO$ & $8$ & external oxygen pool, corresponds to 250$\mu$M. Unchanged from ~\cite{Ebenhoh2014}\\
$\mathrm{Pi_{mol}}$ & $0.01$ & internal pool of phosphates, required to calculate ATP equilibrium\\

\hline\\[-1ex]
\multicolumn{3}{c}{\textbf{Stromal side} [mM]}\\[1ex]
\hline\\[-1ex]
$\aptot$ & $2.55$ & total adenosine phosphate pool (ATP + ADP). Increased, from Bionumbers\\
$\nadptot$ & $0.8$ & NADP + NADPH. Unchanged from ~\cite{Pettersson1988}\\
P$_\textrm{ext}$ & 0.5 & external phosphate. Unchanged from ~\cite{Pettersson1988}\\
CO$_\textrm{2}$ & 0.2 & Unchanged from ~\cite{Pettersson1988} \\
\hline\\[-1ex]
\end{tabular}
\end{center}
\end{table*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table*}
\caption{Rate constants and key parameters of the PETC.}\label{tab:petc:par}
\footnotesize
\begin{center}
\begin{tabular}{lll}
\textbf{Parameter} & \textbf{Value} & \textbf{Description and reference} \\[0.5ex]
\hline\\[-1ex]
% rate constants
$k_\mathrm{PQred}$ & $250\ \mathrm{mmol^{-1}(mol\ Chl)\,s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{PCox}$ & $2500\ \mathrm{mmol^{-1}(mol\ Chl)\,s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{Fdred}$ & $2.5\cdot10^{5}\ \mathrm{mmol^{-1}(mol\ Chl)\,s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{Cytb6f}$ & $2.5\ \mathrm{mmol^{-2}(mol\ Chl)^2\,s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{ATPsynthase}$ & $20\ \mathrm{s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\

$k_H$ & $5\cdot 10^9\ \mathrm{s}^{-1}$ & rate of non-radiative decay. Unchanged from ~\cite{Ebenhoh2014}\\
$k_F$ & $6.25\cdot 10^8\ \mathrm{s}^{-1}$ &  rate of fluorescence. Unchanged from ~\cite{Ebenhoh2014}\\
$k_P$ & $5\cdot 10^9\ \mathrm{s}^{-1}$ & rate of photochemistry. Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{PTOX}$ & $0.01\ \mathrm{mmol^{-1}(mol\ Chl)\,s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{NDH}$ & $0.004\ \mathrm{s}^{-1}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$v^\mathrm{min}_\mathrm{b6f}$ & $-2.5\ \mathrm{mmol(mol\ Chl)^{-1}s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$V^\mathrm{max}_\mathrm{FNR}$ & $ 1500\ \mathrm{mmol(mol\ Chl)^{-1}s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{FQR}$ & $1\ \mathrm{mmol^{-2}(mol\ Chl)^2\,s^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\

$\mathrm{pH_{stroma}}$ & $7.8$ & stroma pH of a dark adapted state. Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{leak}$ & $0.010\ \mathrm{s}^{-1}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$b_\mathrm{H}$ & $100$ & Unchanged from ~\cite{Ebenhoh2014}\\
$\mathrm{HPR}$ & $\frac{14}{3}$ & ratio of protons to ATP in ATP synthase. Unchanged from ~\cite{Ebenhoh2014}\\
\hline\\[-1ex]

% Michaelis constants
\multicolumn{3}{c}{\textbf{Michaelis constants}}\\[1ex]
$K_\mathrm{M,F}$ & $1.56\ \mathrm{mmol(mol\ Chl)^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$K_\mathrm{M,N}$ & $0.22\ \mathrm{mmol(mol\ Chl)^{-1}}$ & Unchanged from ~\cite{Ebenhoh2014}\\
$K_\mathrm{M,ST}$ & 0.2 & Unchanged from ~\cite{Ebenhoh2014}, to yield a PQ redox poise of $\approx$ 1:1\\
$K_\mathrm{M,fdST}$ & 0.5 & Unchanged from ~\cite{Ebenhoh2014}\\
\hline\\[-1ex]
\end{tabular}
\end{center}
\end{table*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
For the CBB cycle parameters, we performed a conversion of the original $V_{max}$ units of $\mu\mathrm{mol\,mg(Chl)^{-1}\,h^{-1}}$ to mM/s The average molar mass of chlorophyll a and b is 895 g/mol, and allows the calculation to a conversion factor (1 $\mu\mathrm{mol\,mg(Chl)^{-1}\,h^{-1}}$ corresponds to 0.25 $\mathrm{mM\,mol(Chl)^{-1}\,s^{-1}}$). 
According to Laisk \cite{laisk2006c3} and Ebenh\"oh \cite{ebenhoh2011minimal}, a concentration of 1 mmol/mol(Chl) in the chloroplast stroma corresponds approximately to a concentration of 0.032 mM. Therefore a rate of 1 $\mu\mathrm{mol^{-1}\,mg(Chl)\,h^{-1}}$corresponds approximately to 0.008 mM/s, which is ultimately the conversion factor applied to the original $V_{max}$ parameters from \cite{Pettersson1988}.
%\captionof{table}{Your caption here}
\begin{table*}
\caption{Rate constants and key parameters of the Calvin Cycle.}\label{tab:cbb:par}
\footnotesize
\begin{center}
\begin{tabular}{lll}
\textbf{Parameter} & \textbf{Value} & \textbf{Description and reference} \\[0.5ex]
\hline\\[-1ex]
\multicolumn{3}{c}{\textbf{$V_{max}$ values of Calvin cycle enzymes}}\\[1ex]
$V_1$ & 2.72 $\mathrm{mM\,s^{-1}}$ & \\
$V_6$ & 1.6 $\mathrm{mM\,s^{-1}}$ &\\
$V_9$ & 0.32 $\mathrm{mM\,s^{-1}}$ &\\
$V_{13}$ & 7.992 $\mathrm{mM\,s^{-1}}$ & \\
$V_{st}$ & 0.32 $\mathrm{mM\,s^{-1}}$ &  \\
$V_x$ & 2 $\mathrm{mM\,s^{-1}}$ &  \\
        \hline\\[-1ex]
\multicolumn{3}{c}{\textbf{Equilibrium constants}}\\[1ex]
$\mathrm{q_2}$ & 3.1 * (10.0 ** (-4.0)) & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_3}$ & 1.6 * (10.0**7.0) & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_4}$ & 22.0 & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_5}$ & 7.1 mM$^{-1}$ & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_7}$ & 0.084 & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_8}$ & 13.0 mM$^{-1}$ & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_{10}}$ & 0.85 & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_{11}}$ & 0.4 & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_{12}}$ & 0.67 & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_{14}}$ & 2.3 & Unchanged from \cite{Pettersson1988}\\
$\mathrm{q_{15}}$ & 0.058 & Unchanged from \cite{Pettersson1988}\\
	\hline\\[-1ex]
\multicolumn{3}{c}{\textbf{Michaelis constants}}\\[1ex]
$K_\mathrm{m1}$ & 0.02 mM& Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{mCO2}$ & 0.0107 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{m6}$ & 0.03 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{m9}$ & 0.013 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{m131}$ & 0.05 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{m132}$ & 0.05 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{m161}$ & 0.014 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{m162}$ & 0.3 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{mst1}$ & 0.08 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{mst2}$ & 0.08 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{pga}$ & 0.25 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{gap}$ & 0.075 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{dhap}$ & 0.077 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{pi}$ & 0.63 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{pxt}$ & 0.74 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i11}$ & 0.04 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i12}$ & 0.04 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i13}$ & 0.075 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i14}$ & 0.9 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i15}$ & 0.07 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i61}$ & 0.7 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i62}$ & 12.0 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i9}$ & 12.0 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i131}$ & 2.0 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i132}$ & 0.7 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i133}$ & 4.0 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i134}$ & 2.5 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{i135}$ & 0.4 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{ist}$ & 10.0 mM & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{ast1}$ & 0.1 & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{ast2}$ & 0.02 & Unchanged from \cite{Pettersson1988}\\
$K_\mathrm{ast3}$ & 0.02 & Unchanged from \cite{Pettersson1988}\\
\hline\\[-1ex]
\end{tabular}
\end{center}
\end{table*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table*}
\caption{Parameters associated with photoprotective mechanisms.}\label{tab:npq:par}
\footnotesize
\begin{center}
\begin{tabular}{lll}
\textbf{Parameter} & \textbf{Value} & \textbf{Description and reference} \\[0.5ex]
\hline\\[-1ex]
\multicolumn{3}{c}{\textbf{Parameters associated with state transitions}}\\[1ex]
\hline\\[-1ex]
$k_\mathrm{Stt7}$ & $0.0035\ \mathrm{s}^{-1}$ & rate of phosphorylation. Unchanged from ~\cite{Ebenhoh2014}\\
$k_\mathrm{Pph1}$ & $0.0013\ \mathrm{s}^{-1}$ & rate of de-phosphorylation. Unchanged from ~\cite{Ebenhoh2014}\\
$\sigma^0_\mathrm{I}$ & 0.37 & relative cross section of PSI-LHCI supercomplex. Changed based on Ünlü \textit{et al.} 10.1073/pnas.1319164111 \\%\cite{Unlu2014, Drop2014}\\
$\sigma^0_\mathrm{II}$ & 0.1 &  relative cross section of PSII. Changed based on Ünlü \textit{et al.} 10.1073/pnas.1319164111\\ %\cite{Unlu2014, Drop2014}\\
$n_\mathrm{ST}$ & 2 & cooperativity. Unchanged from \cite{Ebenhoh2014}\\
$n_\mathrm{fdST}$ & 2 & ad-hoc value to use a reasonable cooperativity. Unchanged from \cite{Ebenhoh2014}\\

\hline\\[-1ex]
\multicolumn{3}{c}{\textbf{Parameters associated with the quencher activity.}}\\[1ex]
\hline\\[-1ex]
\multicolumn{3}{l}{Parameters associated with xanthophyll cycle}\\[1ex]
\hline\\[-1ex]
$k_\mathrm{kDeepoxV}$ & $0.0024\ \mathrm{s}^{-1}$ & fitted previously to keep the ratio of $k_\mathrm{kDeepoxV}$:$k_\mathrm{kEpoxZ}$ =1:10. Unchanged from~\cite{Matuszynska2016} \\
$k_\mathrm{kEpoxZ}$ & $0.00024\ \mathrm{s}^{-1}$ & Unchanged from~\cite{Matuszynska2016}\\
$K_\mathrm{phSat}$ & $5.8$ & half-saturation pH for de-epoxidase activity, highest activity at ~pH 5.8. Unchanged from~\cite{Matuszynska2016}\\
$\mathrm{nH_X}$ & $5$ & Hill-coefficient for de-epoxidase activity \\
$K_\mathrm{ZSat}$ & $0.12$ & half-saturation constant (relative conc. of $\mathrm{Zx}$) for quenching of $\mathrm{Zx}$. Unchanged from~\cite{Matuszynska2016} \\
\hline\\[-1ex]
\multicolumn{3}{l}{Parameters associated with PsbS protonation}\\[1ex]
\hline\\[-1ex]
$\mathrm{nH_L}$ & $3$ & Hill-coefficient for activity of de-protonation. Unchanged from~\cite{Matuszynska2016} \\
$k_\mathrm{Deprotonation}$ & $0.0096\ \mathrm{s}^{-1}$ & rate of PsbS protonation. Unchanged from~\cite{Matuszynska2016}\\
$k_\mathrm{ProtonationL}$ & $0.0096\ \mathrm{s}^{-1}$ & rate of PsbS de-protonation. Unchanged from~\cite{Matuszynska2016}\\
$K_\mathrm{phSatLHC}$ & $5.8$ & pKa of PsbS activation. Unchanged from~\cite{Matuszynska2016}\\
\hline\\[-1ex]
\multicolumn{3}{l}{Previously fitted quencher contribution factors}\\[1ex]
\hline\\[-1ex]
$\gamma_0$ & $0.1$ & contribution of the base quencher, not associated with protonation or zeaxanthin.Unchanged from~\cite{Matuszynska2016}\\
$\gamma_1$ & $0.25$ & fast quenching present due to protonation. Unchanged from~\cite{Matuszynska2016} \\
$\gamma_2$ & $0.6$ & fastest possible quenching. Unchanged from~\cite{Matuszynska2016}\\
$\gamma_3$ & $0.15$ & slow quenching of $\mathrm{Zx}$ present despite lack of protonation. Unchanged from~\cite{Matuszynska2016}\\
\end{tabular}
\end{center}
\end{table*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table*}
\caption{Physical constants and standard potentials}\label{tab:phys:par}
\footnotesize
\begin{center}
\begin{tabular}{lll}
\textbf{Parameter} & \textbf{Value} & \textbf{Description and reference} \\[0.5ex]
\hline\\[-1ex]
\multicolumn{3}{c}{\textbf{Physical constants}}\\[1ex]
\hline\\[-1ex]
$\mathrm{F}$ & $96.485\ \mathrm{kJ}$ & Faraday constant \\ 
$\mathrm{R}$ & $8.3\ \mathrm{JK^{-1}mol^{-1}}$ & universal gas constant \\
$\mathrm{T}$ & $298\ \mathrm{K}$ &temperature\\
\hline\\[-1ex]
\multicolumn{3}{c}{\textbf{Standard potentials}}\\[1ex]
\hline\\[-1ex]
$E^0(\mathrm{QA/QA^-})$ & $-0.140\ \mathrm{V}$ & Unchanged from \cite{Ebenhoh2014}\\
$E^0(\mathrm{PQ/PQH_2})$ & $0.354\ \mathrm{V}$ & Unchanged from \cite{Ebenhoh2014}\\
$E^0(\mathrm{PC/PC^-})$ & $0.380\ \mathrm{V}$ & Unchanged from \cite{Ebenhoh2014}\\
$E^0(\mathrm{P}^+_{700}/\mathrm{P}_{700})$ & $0.480\ \mathrm{V}$ & Unchanged from \cite{Ebenhoh2014}\\
$E^0(\mathrm{FA/FA^-})$ & $-0.550\ \mathrm{V}$ & Unchanged from \cite{Ebenhoh2014}\\
$E^0(\mathrm{Fd/Fd^-})$ & $-0.430\ \mathrm{V}$ & Unchanged from \cite{Ebenhoh2014}\\
$E^0(\mathrm{NADP^+/NADPH})$ & $-0.113\ \mathrm{V}$ & Unchanged from \cite{Ebenhoh2014}\\

\hline\\[-1ex]
$\Delta G_{0_{ATP}}$ & $30.6 \ \mathrm{kJ/mol/RT}$ & standard Gibbs free energy change of ATP formation. Unchanged from \cite{Ebenhoh2014}
\end{tabular}
\end{center}
\end{table*}

\newpage
\subsection{Figures}
Additional figures to support the Results section of the main text.

\setcounter{figure}{0}  
\begin{figure}[h]
	\centering
\includegraphics[width=0.8\linewidth]{PFD_steadystate}
	\caption{Result of the steady state simulation for different light intensities, varied between 0 and 300~\PFD. Displayed are the rates of RuBisCO, starch production and TPT export (in mM/s).}
        \label{fig:si:steadystate}
\end{figure}

\begin{figure}
\centering
        \begin{subfigure}[b]{0.475\textwidth}
            \center
            \includegraphics[width=\textwidth]{bistable_5}
            \caption[]%
            {{\small 20 ~\PFD}}    
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.475\textwidth}  
            \center
            \includegraphics[width=\textwidth]{bistable_1}
            \caption[]%
            {{\small 50 ~\PFD}}    
        \end{subfigure}
        \vskip\baselineskip
        \begin{subfigure}[b]{0.475\textwidth}   
            \centering 
            \includegraphics[width=\textwidth]{bistable_2}
            \caption[]%
            {{\small 100 ~\PFD}}    
        \end{subfigure}
        \quad
        \begin{subfigure}[b]{0.475\textwidth}   
            \centering 
            \includegraphics[width=\textwidth]{bistable_3}
            \caption[]%
            {{\small 200 ~\PFD}}    
        \end{subfigure}
        \caption{Simulations in different light intensities for different initial concentrations of RU5P, ranging from 0.1 to 0.6 mM. The RU5P abundance is displayed after 3 s, close to the beginning of equilibration. The dashed line displays the critical concentration of RU5P for sufficient cyclic activity after equilibrating. The critical concentration remains the same in different light intensities and energy abundance.} 
        \label{fig:si:bistability}
    \end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{oxPPP}
	\caption{Steady state simulations in low light intensity of 5 ~\PFD and systematically increasing influxes of RU5P from 0. to 0.08 mM/s. The figure displays rates of RuBisCO and triose phosphate export. }
        \label{fig:si:Ru5Pinflux}
\end{figure}

\begin{figure}
	\centering
\includegraphics[width=0.8\linewidth]{standby_modus}
	\caption{Simulations of light-dark-light transitions for different light intensities, ranging from 20-200~\PFD with a constant RU5P influx of 5 $\mu$M/s. Shown are the dynamics of internal orthophosphate concentration, triose phosphate export and carbon fixation rates. The simulated time-courses are shown from 200s, when the system has reached a stationary state. From 300-1300s (grey area), the external light has been set to 5~\PFD. The figure illustrates that even after long periods of low light intensities the CBB cycle is able to restart in the second light period if a small constant influx of RU5P is present. }
        \label{fig:si:standby}
\end{figure}

\begin{figure*}[h]
\begin{center}
        \begin{subfigure}[b]{0.475\textwidth}
            \includegraphics[width=\textwidth]{Ferredoxin}
            \caption[]%
            {{\small Ferredoxin abundance}}    
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.475\textwidth}  
            \includegraphics[width=\textwidth]{NADP}
            \caption[]%
            {{\small relative NADP abundance}}    
        \end{subfigure}
        \vskip\baselineskip
        \begin{subfigure}[b]{0.475\textwidth}   
            \includegraphics[width=\textwidth]{PQ}
            \caption[]%
            {{\small relative ox. Plastoquinone abundance}}    
        \end{subfigure}
        \quad
        \begin{subfigure}[b]{0.475\textwidth}   
            \includegraphics[width=\textwidth]{Violaxanthin}
            \caption[]%
            {{\small relative Violaxanthin abundance}}    
        \end{subfigure}
        \caption{3D display of the steady state analysis of the system under varying light intensities (x-axis) and carbon fixation velocities (y-axis). On the z-axis: (a) the Ferredoxin abundance, (b) relative ATP abundance, (c) relative ox. Plastoquinone abundance, and (d) relative Violaxanthin abundance are displayed.}
\label{fig:surfacePlots}
\end{center}
    \end{figure*}

\begin{figure}
	\centering
\includegraphics[width=0.8\linewidth]{50pfd_FCC}
	\caption{Flux control coefficients of the system in 50~\PFD light and unchanged carbon fixation activity ($f_\mathrm{CBB} = 1$). The parameters $\mathrm{PSII^{tot}}$ and $\mathrm{PSI^{tot}}$ denote the  $V_{\mathrm{max}}$-values of Photosystem II and Photosystem I, respectively. They exhibit more flux control than other reactions.}
        \label{fig:si:fcc50}
\end{figure}
\begin{figure}
	\centering
\includegraphics[width=0.8\linewidth]{300pfd_FCC}
 	\caption{Flux control coefficients of the system in 300~\PFD light and unchanged carbon fixation activity ($f_\mathrm{CBB} = 1$). The parameter $V_9$ denotes the  $V_\mathrm{max}$-value of the SBPase and exhibits more flux control than other reactions.}
        \label{fig:si:fcc300}
\end{figure}
